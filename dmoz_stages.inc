<?php
/**
 * @file
 * Dmoz gateway for Llengua.org
 *
 * Developed by Jose A. Reyero, http://www.reyero.net
 *  for Interactors, http://www.interactors.coop
 *
 * Some code from PHParadise, http://phparadise.de
 */

/**
 * Check status and start next stage if finished
 */
function dmoz_control($next){
  if(_dmoz_status()){
     dmoz_stage('start', $next);
  }
}

/**
 * Process step by step
 */

/**
 * Special stage: Stop
 *
 * Process disabled
 */
function dmoz_stage_stop($op){
  // Do nothing, just wait
}

/**
 * Special stage: Idle
 *
 * Wait until next update.
 * We need to manage remote times because of time zones
 */
function dmoz_stage_idle($op){
  switch($op){
    case 'start': // Check for updates
      variable_set('dmoz_last_process',time());
      $updated = min(
        _dmoz_remote_last_modified(variable_get('dmoz_content_url', 'http://rdf.dmoz.org/rdf/content.rdf.u8.gz')),
        _dmoz_remote_last_modified(variable_get('dmoz_struct_url', 'http://rdf.dmoz.org/rdf/structure.rdf.u8.gz'))
      );
      if($updated > variable_get('dmoz_last_update', 0)){
        // Go for it
        variable_set('dmoz_last_update', $updated);
        dmoz_stage('start', 'download_structure');
      }
      break;
    case 'cron':
      // Check for updates only once every.. hour ?
      if(time() - variable_get('dmoz_last_process',0) > 3600){
        _dmoz_log("Its time to check for new files...");
        dmoz_stage('start');
      }
      break;
  }
}

/**
 * Check when a remote file was last modified
 *
 * Returns time
 * Uses PHParadise, remote last modified
 */
function _dmoz_remote_last_modified($url){
  _dmoz_log("Checking remote file $url");
  $data = get_header_array($url);
  $last = strtotime($data['Last-Modified']);
  _dmoz_log("File $url Last Modified ".format_date($last));
  // Returns 0 if everything fails
  return is_numeric($last)? $last : 0;
}

/**
 * Special stage: Error
 *
 * An error has occurred, stop waiting human intervention
 */
function dmoz_stage_error($op){
  switch($op) {
    case 'start':
      if($text = _dmoz_status('error contents')){
        _dmoz_watchdog('Dmoz processing error:<br/>'.$text, WATCHDOG_ERROR);
      }
      break;
    case 'cron':
      // Do nothing. Just wait.
  }
}

/**
 * Download.
 * Pre: Delete files
 * End: Files completed
 */
function dmoz_stage_download_structure($op){
  switch($op){
    case 'start': // Start download
      _dmoz_download(variable_get('dmoz_struct_url', 'http://rdf.dmoz.org/rdf/structure.rdf.u8.gz'));
      break;
    case 'cron': // Check end of download
      dmoz_control('download_content');
      break;
  }
}

function dmoz_stage_download_content($op){
  switch($op){
    case 'start': // Start download
      _dmoz_download(variable_get('dmoz_content_url', 'http://rdf.dmoz.org/rdf/content.rdf.u8.gz'));
      break;
    case 'cron': // Check end of download
      dmoz_control('expand_structure');
      break;
  }
}

/**
 * Expand. Uncompress file.
 *
 * Pre: Delete files
 * End: Files created
 */
function dmoz_stage_expand_structure($op){
  switch($op){
    case 'start': // Start download
      $file = _dmoz_file(basename(variable_get('dmoz_struct_url', 'http://rdf.dmoz.org/rdf/structure.rdf.u8.gz')));
      _dmoz_expand($file);
      break;
    case 'cron': // Check end of download
      dmoz_control('expand_content');
      break;
  }
}

function dmoz_stage_expand_content($op){
  switch($op){
    case 'start': // Start download
      $file = _dmoz_file(basename(variable_get('dmoz_content_url', 'http://rdf.dmoz.org/rdf/content.rdf.u8.gz')));
      _dmoz_expand($file);
      break;
    case 'cron': // Check end of download
      dmoz_control('extract_structure');
      break;
  }
}

/**
 * Data Extraction
 *
 * Pre: Delete extracted files
 * End: Files created
 */
function dmoz_stage_extract_structure($op){
  switch($op) {
    case 'start':
      $filein = _dmoz_file(variable_get('dmoz_struct_file', 'structure.rdf.u8'));
      $fileout = _dmoz_file(variable_get('dmoz_struct_extract', 'extracted-structure.rdf.u8'));
      _dmoz_exec('extract.pl', variable_get('dmoz_category', ''), $filein, $fileout);
      break;
    case 'cron':
      dmoz_control('extract_content');
      break;
  }
}

function dmoz_stage_extract_content($op){
  switch($op){
    case 'start':
      $filein = _dmoz_file(variable_get('dmoz_content_file', 'content.rdf.u8'));
      $fileout = _dmoz_file(variable_get('dmoz_content_extract', 'extracted-content.rdf.u8'));
      _dmoz_exec('extract.pl', variable_get('dmoz_category', ''), $filein, $fileout);
      break;
    case 'cron':
      dmoz_control('import_structure');
      break;
  }
}

function dmoz_stage_import_structure($op){
  switch($op){
    case 'start':
      global $config;
      $datafile = _dmoz_file(variable_get('dmoz_struct_extract', 'extracted-structure.rdf.u8'));
      $configfile = "$config/settings.php";
      _dmoz_exec('structure2db.pl', $datafile, $configfile);
      break;
    case 'cron':
      dmoz_control('import_content');
      break;
  }
}

function dmoz_stage_import_content($op){
  switch($op){
    case 'start':
      global $config;
      $datafile =  _dmoz_file(variable_get('dmoz_content_extract', 'extracted-content.rdf.u8'));
      $configfile = "$config/settings.php";
      return _dmoz_exec('content2db.pl', $datafile, $configfile);
      break;
    case 'cron':
      dmoz_control('process_structure');
      break;
  }
}

/**
 * Create categories and path aliases
 */
function dmoz_stage_process_structure($op){
  switch($op){
    case 'cron':
      dmoz_timer(); // Start
      $limit = variable_get('dmoz_process_rows', 100);
      $vid = variable_get('dmoz_vocabulary', '');
      $count = 0;
      // TODO: Lock tables
      $result = db_query_range('SELECT * FROM {dmoz_structure} WHERE processed=0', 0, $limit);
      while($dmoz = db_fetch_object($result)) {
        _dmoz_log("DEBUG: processing title=$dmoz->title catid=$dmoz->catid topic=$dmoz->topic");
        $dmoz->vid = $vid;
        dmoz_category_update($dmoz);
        // Mark as processed
        db_query("UPDATE {dmoz_structure} SET processed=1 WHERE catid=%d", $dmoz->catid);
        dmoz_stats('category', 'processed');
        $count++;
      }
      dmoz_stats('category', 'save');
      // Flush messages, taxonomy updates, etc..
      drupal_get_messages();
      _dmoz_watchdog("Processing structure: $count records, time=".dmoz_timer().", ".dmoz_stats('category', 'string'));
      _dmoz_log("STATS:Global: ". dmoz_stats('category', 'string', TRUE));
      // End of stage
      if(!$count){
        dmoz_stage('start', 'process_content');
      }
      break;
  }
}

/**
 * Create link nodes
 */
function dmoz_stage_process_content($op){
  switch($op){
    case 'cron':
      dmoz_timer(); // Start
      $limit = variable_get('dmoz_process_rows', 100);
      $vid = variable_get('dmoz_vocabulary', '');
      $count = 0;
      $result = db_query_range('SELECT * FROM {dmoz_xurls} WHERE processed=0', 0, $limit);
      while($dmoz = db_fetch_object($result)) {
        _dmoz_log("DEBUG: processing url=$dmoz->url title=$dmoz->title catid=$dmoz->catid");
        dmoz_content_update($dmoz);
        // Mark as processed
        db_query("UPDATE {dmoz_xurls} SET processed=1 WHERE urlid=%d", $dmoz->urlid);
        dmoz_stats('content', 'processed');
        $count++;
      }
      dmoz_stats('content', 'save');
      // Flush messages
      drupal_get_messages();
      //print theme('status_messages');
      _dmoz_watchdog("Processing content: $count records, time=".dmoz_timer().", ".dmoz_stats('content', 'string'));
      _dmoz_log("STATS:Global: ". dmoz_stats('content', 'string', TRUE));
      // End of stage
      if(!$count){
        dmoz_stage('start', 'cleanup');
      }
      break;

  }
}

/**
 * Final cleanup
 * - Delete not refreshed/updated nodes, also removed in dmoz
 * - Delete not refreshed/updated categories, (and empty categories?)
 */
function dmoz_stage_cleanup($op){
  switch($op){
    case 'cron':
      dmoz_timer(); // Start
      $limit = variable_get('dmoz_process_rows', 100);
      $vid = variable_get('dmoz_vocabulary', '');
      $lastupdate = variable_get('dmoz_last_update', 0);
      $count = 0;

      // Cleanup deleted links
      $result = db_query_range('SELECT nid FROM {dmoz_node} WHERE lastrefresh < %d', $lastupdate, 0, $limit);
      while($node = db_fetch_object($result)) {
        _dmoz_log("DEBUG: deleting node $node->nid");
        node_delete(array('nid' => $node->nid, 'confirm' => 1));
        $count++;
      }
      if($count) {
        _dmoz_watchdog("Clean up, deleted old links: $count nodes, time=".dmoz_timer());
        break;
      }

      // Cleanup deleted categories
      $result = db_query_range('SELECT * FROM {dmoz_term_data} WHERE lastrefresh < %d', $lastupdate, 0, $limit);
      while($cat = db_fetch_object($result)){
        _dmoz_log("DEBUG $count: deleting category (term) $cat->tid $cat->name");
        taxonomy_del_term($cat->tid);
        $count++;
      }
      if($count) {
        _dmoz_watchdog("Clean up, deleted old categories: $count terms, time=".dmoz_timer());
        break;
      }

      // Cleanup empty categories ?
      /*
      $result = db_query_range('SELECT * FROM {term_data} WHERE vid = %d AND tid NOT IN (SELECT parent FROM {term_hierarchy}) AND tid NOT IN (SELECT tid FROM {term_node})', $vid, 0, $limit);
      while($cat = db_fetch_object($result)){
        _dmoz_log("DEBUG $count: deleting category (term) $cat->tid $cat->name");
        $count++;
      }
      */

      // If we reach here and no rows have been processed, it is the End of stage
      if(!$count){
        dmoz_stage('start', 'recount');
      }
      break;
  }
      // Flush messages (comment out for debugging)
      drupal_get_messages();

}

/**
 * Last step: Recount links in categories
 */
function dmoz_stage_recount($op){
  switch($op){
    case 'start':
      // Reset fields for recount = number of direct nodes
      db_query("UPDATE {dmoz_term_data} d SET recount = -1");
      variable_set('dmoz_recount_tid', 0);
      break;
    case 'cron':
      dmoz_timer(); // Start
      $limit = variable_get('dmoz_process_rows', 100);
      $vid = variable_get('dmoz_vocabulary', '');
      $tid = variable_get('dmoz_recount_tid', 0);
      $lastupdate = variable_get('dmoz_last_update', 0);
      $count = dmoz_recount(0);
      if($count == 1){
        dmoz_stage('start', 'idle');
      } else {
        _dmoz_watchdog("Recounting links by category: $count terms processed, time=".dmoz_timer());
      }
      break;

  }
}

/**
 * Recursive function to count nodes.
 * Returns number of processed rows. Simple tree traversal algorithm
 */
function dmoz_recount($tid){
  $rows = 1; // Number of rows processed
  $count = 0;
  $limit = variable_get('dmoz_process_rows', 100);
  //$limit = 10;

  //_dmoz_log("DEBUG: Recounting category tid=$tid rows=$rows");

  $result = db_query('SELECT t.tid FROM {term_hierarchy} h, {dmoz_term_data} t WHERE h.parent = %d AND h.tid = t.tid AND t.recount = -1', $tid);

  // Recount subtree
  while( ($rows < $limit ) && ($cat = db_fetch_object($result))){
    $rows += dmoz_recount($cat->tid);
  }

  if($tid && ($rows < $limit)) {
    // Subtree successfully recounted: update this one
    $count += db_result(db_query('SELECT SUM(t.count) FROM {term_hierarchy} h, {dmoz_term_data} t WHERE h.parent = %d AND h.tid = t.tid', $tid));
    $count += db_result(db_query('SELECT COUNT(*) FROM {term_node} WHERE tid = %d', $tid));
    db_query('UPDATE {dmoz_term_data} SET count = %d, recount = %d WHERE tid = %d', $count, $count, $tid);
    _dmoz_log("DEBUG: Updating node count: tid=$tid count=$count");
  } else {
    _dmoz_log("DEBUG: NOT updating node count: tid=$tid rows=$count");
  }
  return $rows;
}

/**
 * Gets parent category using 'topic'
 */
function dmoz_category_get_parent(&$dmoz){
    return dmoz_get_category(dmoz_topic_get_parent($dmoz->topic), 'topic' );
}

/**
 * Trims last word to get the parent topic
 */
function dmoz_topic_get_parent($topic){
  return substr($topic, 0, strrpos($topic, '/'));
}

/**
 * Update category
 */
function dmoz_category_update($dmoz){
  // First, get rid of one letter categories in the middle
  $dmoz->topic = preg_replace("'/\w/'",'/',$dmoz->topic);
  $output = "DEBUG: dmoz_category_update $dmoz->topic";
  if($data = dmoz_get_category($dmoz->catid, 'catid', TRUE)){
    // Update
    $output .= " Update ";
    // Check for changes
    $changed = array();
    if($data->name != $dmoz->title){
      $data->name = $dmoz->title;
      $changed[] = 'title';
    }
    if($data->topic != $dmoz->topic){
      $changed[] = 'topic';
      $data->topic = $dmoz->topic;
    }
    if($data->description != $dmoz->description) {
      $changed[] = 'description';
      $data->description = $dmoz->description;
    }

    if(count($changed)){
      dmoz_stats('category', 'updated');
      $output .=  " UPDATE ". implode(', ', $changed);
    } else {
      // No changes, update lastrefresh
      db_query("UPDATE {dmoz_term_data} SET lastrefresh=%d WHERE catid=%d", time(), $dmoz->catid);
      $output .=  " unchanged ";
      $data = NULL;
    }

  } elseif(strlen(trim($dmoz->title)) == 1){
      // One letter category: group with parent and reassign nodes
      $parent = dmoz_category_get_parent($dmoz);
      // Update links with old category
      $result = db_query("UPDATE {dmoz_xurls} SET catid=%d WHERE catid=%d", $parent->catid, $dmoz->catid);
      $number = db_affected_rows($result);
      $output .= " One letter, reassigning $number links to parent: catid=$parent->catid topic=$parent->topic ";
      // Update children, there are some, this dirty dmoz directory.. :-(
      // Change children from /Parent/x/topic to /Parent/topic
      /*
      $result = db_query("SELECT * FROM {dmoz_structure} WHERE catid != %d AND topic LIKE '%s'", $dmoz->catid, $dmoz->topic.'%');
      $output .= " ";
       while($child = db_fetch_object($result)){
        $output .= " updating $child->topic to ".$parent->topic.'/'.$child->title;
        db_query("UPDATE {dmoz_structure} SET topic='%s' WHERE catid=%d", $parent->topic.'/'.$child->title, $child->catid);
      }
      */
      dmoz_stats('category', 'reassigned');
      $data = NULL;
  } else {
    // Create new category
    $output .=  " NEW ";
    $data = $dmoz;
    $data->name = $dmoz->title;
    dmoz_stats('category', 'created');
  }

  // Save if new / updated
  if($data){
    $data->lastrefresh = $data->lastupdate = time();
    // Check for parent
    if($parent = dmoz_category_get_parent($data)) {
        $data->parent = array($parent->tid);
    } else {
      _dmoz_watchdog("Category without parent catid=$dmoz->catid topic=$dmoz->topic",WATCHDOG_ERROR);
    }
    taxonomy_save_term(object2array($data));
  }
  // Mark as processed

  _dmoz_log($output);
}

/**
 * Update content
 */
function dmoz_content_update($dmoz){
  $output = "DEBUG: dmoz_content_update ";
  $category = dmoz_get_category($dmoz->catid);
  if($data = dmoz_get_content($dmoz, TRUE)){
    // Check for changes
    $changed = array();
    if($data->title != $dmoz->title){
      $data->title = $dmoz->title;
      $changed[] = 'title';
    }
    if($data->body != $dmoz->description){
      $changed[] = 'description';
      $data->body= $dmoz->description;
    } 
    if($data->body != $dmoz->description) {
      $changed[] = 'description';
      $data->description = $dmoz->description;
    }
    // A node may have other categories assigned
    if(!$data->taxonomy || !in_array($category->tid, $data->taxonomy)){
      $changed[] = 'category';
      $data->taxonomy = array($category->tid);
    }

    if(count($changed)){
      dmoz_stats('content', 'updated');
      $output .=  " UPDATE ". implode(', ', $changed);
    } else {
      // No changes, update last refresh
      db_query("UPDATE {dmoz_node} SET lastrefresh=%d WHERE nid=%d", time(), $data->nid);
      $output .=  " REFRESH ";
      $data = NULL;
    }
  } elseif($category->tid) {
    // Create new node
    $output .=  " NEW ";
    $data = $dmoz;
    $data->type = 'dlink';
    $data->body = $dmoz->description;
    $data->taxonomy = array($category->tid);
    dmoz_stats('content', 'created');
  } else {
    _dmoz_watchdog("Link without category urlid=$dmoz->urlid catid=$dmoz->catid", WATCHDOG_ERROR);
  }

  // Save if new / updated
  if($data){
    $data->urltype = $dmoz->type ? $dmoz->type : 'link';
    $data->teaser = $dmoz->description;
    $data->lastrefresh = $data->lastupdate = time();
    node_save($data);
  }
  // Mark as processed

  _dmoz_log($output);
}

/**
 * Finds existing links by url, catid
 * There may be duplicated urls, that's why we search also by catid
 * If node is in a different category then we'll create a new one. The old one will be deleted in the cleanup
 */
function dmoz_get_content($data, $taxonomy = TRUE){
  $nid = db_result(db_query("SELECT nid FROM {dmoz_node} WHERE url = '%s' AND catid = %d",  $data->url, $data->catid));
  if(is_numeric($nid) && $node =node_load(array('nid' => $nid))){
    if($taxonomy) $node->taxonomy = array_keys(taxonomy_node_get_terms($node->nid));
    return $node;
  } else {
    return NULL;
  }
}

/**
 * Simple timer
 */
function dmoz_timer(){
  static $timer = 0;
  list($usec, $sec) = explode(' ', microtime());
  $current = (float)$usec + (float)$sec;
  $diff = $current - $timer;
  $timer = $current;
  return $diff;
}

/**
 * Keeps general statistics about data
 */
function dmoz_stats($type, $op = 'get', $total = FALSE){
  static $stats = array();
  static $request = array();
  if(!isset($stats[$type])){
    $stats[$type] = variable_get("dmoz_stats_$type", array());
    $request[$type] = array();
  }
  switch($op){
    case 'save':
      variable_set("dmoz_stats_$type", $stats[$type]);
      break;
    case 'get':
      return $total ? $stats[$type] : $request[$type];
      break;
    case 'string':
      $data = $total ? $stats[$type] : $request[$type];
      $output = '';
      foreach($data as $op => $value){
        $output .= "<strong>$op</strong>=$value&nbsp";
      }
      return $output;
      break;
    default: // case 'updated': case 'deleted': case 'created':  case 'processed':
      $stats[$type][$op] += 1;
      $request[$type][$op] += 1;
      return $total ? $stats[$type][$op] : $request[$type][$op];
      break;
  }
}

/**
 * Expand file
 */
function _dmoz_expand($file){
  $output = _dmoz_exec('unzip.pl', $file);
  _dmoz_log("DEBUG:dmoz_expand: file=$file output=$output");
}

/**
 * Download file
 */
function _dmoz_download($url){
  $folder = variable_get('dmoz_dir_working', 'files/dmoz');
  _dmoz_exec('download.pl', $url, $folder);
  _dmoz_log("DEBUG:dmoz_download url=$url output=$otput");
}

/**
 * Handles command execution and signaling
 */
function _dmoz_exec(){
  $args = func_get_args();
  $command = DMOZ_PERL .' '.variable_get('dmoz_dir_scripts', 'modules/dmoz/scripts') .'/'. array_shift($args);
  //$command = realpath($command);
  $params = implode(' ', $args);

  // $cwd = realpath(variable_get('dmoz_dir_working', 'files/dmoz'));

  // Redirect stdout, stderr and run in background.
  $control = _dmoz_status('command').' &';
  $commandline = "$command $params $control";
  $output = exec($commandline);
  _dmoz_log("DEBUG:dmoz_exec: command=$commandline output=$output");
}

/**
 * Simple inter process signaling through output.txt and error.txt files
 * $op = check | reset | command | output file | error file | output contents | error contents 
 */
function _dmoz_status($op = 'check'){
  $file = _dmoz_file('output.txt');
  $error = _dmoz_file('error.txt');
  switch($op){
    case 'output file':
      return $file;
    case 'error file':
      return $error;
    case 'command': // Returns exit condition for commands
      return '>'.$file.' 2>'.$error;
    case 'reset':
      if(file_exists($file) ) unlink($file);
      if(file_exists($error) ) unlink($error);
      return TRUE;
    case 'check':
      // Check for error file first, then for output file
      if(file_exists($error) && $contents = file_get_contents($error)){
        _dmoz_log("Error file $contents");
        dmoz_stage('start', 'error');
        return FALSE;
      }elseif(file_exists($file) && $contents = file_get_contents($file)) {
        if(preg_match("/.*OK$/",$contents)) {
          return TRUE;
        }
      }
      break;
    case 'output contents':
      if(file_exists($file)){
        return file_get_contents($file);
      }
      break;
    case 'error contents':
      if(file_exists($error)){
        return file_get_contents($error);
      }
      break;

  }
  return FALSE;
}

/*------------------------------------------------------------------------------
|
|                             PHParadise source code
|
|-------------------------------------------------------------------------------
|
| file:             remote last modified
| category:         date and time
|
| last modified:    Mon, 20 Jun 2005 16:40:28 GMT
| downloaded:       Tue, 20 Dec 2005 17:59:39 GMT as PHP file
|
| code URL:
| http://phparadise.de/php-code/date-and-time/remote-last-modified/
|
| description:
| functions to retrieve the last modified date from files on remote servers. very
| practical to check if a new version of a file is ready to download on another
| server.
|
------------------------------------------------------------------------------*/


function get_raw_header($host,$doc)
{
	$httpheader = '';
	$fp = fsockopen ($host, 80, $errno, $errstr, 30);
	if (!$fp)
	{
		echo $errstr.' ('.$errno.')';
	}else{
		fputs($fp, 'GET '.$doc.' HTTP/1.0'."\r\n".'Host: '.$host."\r\n\r\n");
		while(!feof($fp))
		{
			$httpresult = fgets ($fp,1024);
			$httpheader = $httpheader.$httpresult;
			if (ereg("^\r\n",$httpresult))
			break;
		}
		fclose ($fp);
	}
	return $httpheader;
}
function get_header_array($url)
{
	$url = ereg_replace('http://','',$url);
	$endHostPos = strpos($url,'/');
	if(!$endHostPos) $endHostPos = strlen($url);
	$host = substr($url,0,$endHostPos);
	$doc = substr($url,$endHostPos,strlen($url)-$endHostPos);
	if($doc == '') $doc = '/';
	$raw = get_raw_header($host,$doc);
	$tmpArray = explode("\n",$raw);
	for ($i=0;$i<sizeof($tmpArray); $i++)
	{
		@list($name, $value) = explode(':', $tmpArray[$i], 2);
		$array[trim($name)]=trim($value);
	}
	return $array;
}

?>
