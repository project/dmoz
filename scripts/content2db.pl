#!/usr/bin/perl
#
#
# content2db.pl drupal version 1.1
#
# A quick* and dirty program to parse the dmoz/ODP content RDF file.
#
# Notes:
# 1. The category records are ignored as this information is duplicated in 
# the structure.rdf.u8 file. Only the external page records are loaded.
#
# * "quick" does not refer to run time. Expect a long wait for a full
# content file to be loaded into the database!
#
# History
#
# 2005 Dec 10 - Addapted for Drupal integration by Jose A. Reyero, http://www.reyero.net
#
# 2004 Feb 20 - v1.1 Added patches from will@hellacool.co.uk that simplify
#                    the DBI code by using placeholders.
#
#==========================================================================
# Copyright (c) 2003-2004 by R. Steven Rainwater <steve@ncc.com>
# 
# http://www.ncc.com/freeware/
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
#==========================================================================
#
# This file has been modified to suit the needs of phpODPWorld. All changes
# are licensed under GNU GPL too.
#
# Copyright (c) 2005 by Hans Fredrik Nordhaug <hansfn@gmail.com>
#
use strict;
use bytes;
use XML::Parser;
use DBI;

$| =1;

if ($#ARGV != 1){
    print "Usage: content2db.pl datafile configfile \n";
    exit;
}
my $datafile = $ARGV[0];
my $configfile = $ARGV[1];

my $DEBUG = 1;

print "content2db.pl\n" if $DEBUG;

# Global database config values
our $db_type;
our $db_name;
our $db_passwd;
our $db_user;
our $db_server;

# Get db config from Drupal settings file
# $db_url = 'db_type://db_user:db_passwd@db_server/db_name';

open (INPUT, $configfile) 	|| die "can't open $configfile: $!";

# Parses line by line till $db_url is found
while ( <INPUT>) {
	chomp;
	# do something with $_
	if( m/^\s*\$db_url\s*\=\s*[\'\"](\w+)\:\/\/(\w+)\:(\w+)\@(\w+)\/(\w+).*/i) {
		# print "match: $_\n";
		$db_type = $1;
		$db_user = $2;
		$db_passwd = $3;
		$db_server = $4;
		$db_name = $5;
		last; # Exit loop
	}
}

close(INPUT)	    	|| die "can't close $configfile: $!";

# Debug Information
if($DEBUG){
  print "Drupal settings:\n";
  print "db_type=   $db_type\n";
  print "db_user=   $db_user\n";
  print "db_passwd= $db_passwd\n";
  print "db_server= $db_server\n";
  print "db_name=   $db_name\n";
}

# Globals containing the current category information
my $catid = '';
my $ages = '';
my $priority = '';
my $mediadate = '';
my $type = '';
my $url = '';
my $title = '';
my $description = '';

my $records = 0;

# Sort of an XML tag stack thingy
my @pendingTag = ();

# connect to database
my $dsn;
if ($db_type == "Pg") {
    $dsn = "dbi:$db_type:dbname=$db_name";
} elsif ($db_type == "mysql"){
    $dsn = "dbi:$db_type:database=$db_name";
} else {
    print "Unknown database type - change the value for \$dsn to suit your type/driver\n";
    $dsn = "dbi:$db_type:$db_name";
}
my $dbh = DBI->connect($dsn,$db_user,$db_passwd)
or die "Can't connect to database: $DBI::errstr\n";

# purge any existing content data
$dbh->do("DELETE FROM dmoz_xurls") or print "\nDelete Failed: $DBI::errstr\n";

# prepare a db handle with the insert statement with placeholders
my $sth = $dbh->prepare("INSERT INTO dmoz_xurls(catid, priority, ages, mediadate, type, url, title, description) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

# initialize XML parser
my $p1 = new XML::Parser(Style => 'Stream',
                      ErrorContext => 2,
		      ProtocolEncoding => 'UTF-8',
		      NoExpand => 1,
		      ParseParamEnt => 0);
# parse the file
$p1->parsefile($datafile);
$dbh->disconnect or die "Disconnect failed: $DBI::errstr\n";
# success
print "OK";
exit;

# XML processing subs

sub StartDocument {
  print "\nLoading content records\n";
  return;
}

sub StartTag {
  m/^<(\S+)[\s>].*/;
  my $tag = $1;
  unshift @pendingTag, $tag;
  if($tag eq 'Topic') { $catid = ''; }
  elsif($tag eq 'ExternalPage') {
    m/.*"(.+)">$/;
    $url = $1;
    $title = '';
    $description = '';
    $ages = '';
    $type = '';
    $priority = '';
    $mediadate = '';
  }
  elsif($tag eq 'RDF' || $pendingTag[1] eq 'Topic' || $pendingTag[1] eq 'ExternalPage') { return; }
  else { print "Warning: Unhandled Tag [$tag] (Pending [$pendingTag[1]])\n"; }
}

sub EndTag {
  m/^<\/(\S+)>$/;
  my $tag = $1;
  shift @pendingTag;
  if($tag eq 'ExternalPage') {
    # insert this topic block into database
    $sth->execute($catid, $priority || 0, $ages || '', $mediadate || '', $type || '', $url, $title, $description)
    or print 
      "\n------\n",
      "Structure Insert Failed: $DBI::errstr\n",
      "Catid: $catid\n",
      "Title: $title\n",
      "URL: $url\n",
      "Type: $type\n",
      "Desc: $description\n",
      "Ages: $ages\n",
      "Priority: $priority\n",
      "Media Date: $mediadate\n";
      
    $records++;
    unless($records % 1000) { print "Record: $records\n"; }
  }
}

sub Text {
  if($pendingTag[0] eq 'd:Title') { $title = $_; }
  elsif($pendingTag[0] eq 'd:Description') { $description = $_; }
  elsif($pendingTag[0] eq 'catid') { $catid = $_; }
  elsif($pendingTag[0] eq 'type') { $type = $_; }
  elsif($pendingTag[0] eq 'priority') { $priority = $_; }
  elsif($pendingTag[0] eq 'mediadate') { $mediadate = $_; }
  elsif($pendingTag[0] eq 'ages') { $ages = $_; }
}

sub PI {
  return;
}

sub EndDocument {
  print "$records loaded\n";
  return;
}
