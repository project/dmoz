#!/usr/bin/perl
#
# This file is part of phpODPWorld and released under GNU GPL.
#
#

use strict;
use DMOZ::ParseRDF;
use File::Temp qw/ tempfile /;


if ($#ARGV != 2){
    print "Usage: extract.pl category rdffile outputfile\n";
    print "(The RDF file should already be uncompressed.)\n";
    exit;
}
my $category = $ARGV[0];
my $rdffile = $ARGV[1];
my $outputfile = $ARGV[2];

print "category = $category\n";
print "rdffile = $rdffile\n";
print "outputfile = $outputfile\n";
#our $category;
#our $extract_basename;
#if ( -e $configfile ) {
#    my $ret = do "$configfile";
#    if (!defined $ret) {
#        die("Couldn't read config file ($configfile)\n"); 
#    }
#} else {
#    die("Config file ($configfile) doesn't exist\n");
#}

if (! -e $rdffile ) {
    die("RDF file ($rdffile) doesn't exist\n");
}

# Determine type of RDF file (based on filename) - "structure" or "content"
my $type;
if ($rdffile =~ /structure/i) {
    $type = "structure";
} elsif ($rdffile =~ /content/i) {
    $type = "content";
} else {
    $type = "unknown";
}


# Creating temp file where the RDF is extracted to
(my $fh, my $tempname) = tempfile();

my $dmoz = DMOZ::ParseRDF->new();
# The RDF is already uncompressed.
$dmoz->gzip_stream(0);
$dmoz->data($rdffile);
$dmoz->parts({ "$category" => "$tempname" });
$dmoz->parse() or die $dmoz->error();

# Fixing the extracted RDF
open(INFILE, $tempname) or die "Can't open newly extracted (temp) RDF file (for reading): $!";
open(OUTFILE, ">$outputfile") or die "Can't open file extracted RDF file (for writing): $!";

print OUTFILE '<?xml version="1.0" encoding="UTF-8" ?>
<RDF xmlns:r="http://www.w3.org/TR/RDF/"
     xmlns:d="http://purl.org/dc/elements/1.0/"
     xmlns="http://dmoz.org/rdf">

';
# Inserting the extracted RDF
while (<INFILE>) {
    print OUTFILE $_;
}
print OUTFILE '</RDF>';

close(INFILE);
close(OUTFILE);

unlink($tempname);

# Success. Give final OK.
print "OK";
exit;
