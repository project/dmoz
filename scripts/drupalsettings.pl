#!/usr/bin/perl

# drupalsettings.pl
# 
# Parse Drupal settings file and load into PERL variables
# Parameters are passed as global variables
#	Params in: $configfile
#   Params out: $db_type, $db_user, $db_passwd, $db_server, $db_name
#
# Developed by Jose A. Reyero, http://www.reyero.net, 2005
# for Interactors, http://www.interactors.coop

# Get db config from Drupal settings file
# $db_url = 'db_type://db_user:db_passwd@db_server/db_name';

open (INPUT, $configfile) 	|| die "can't open $configfile: $!";

# Parses line by line till $db_url is found
while ( <INPUT>) {
	chomp;
	# do something with $_
	if( m/^\s*\$db_url\s*\=\s*[\'\"](\w+)\:\/\/(\w+)\:(\w+)\@(\w+)\/(\w+).*/i) {
		# print "match: $_\n";
		$db_type = $1;
		$db_user = $2;
		$db_passwd = $3;
		$db_server = $4;
		$db_name = $5;
		last; # Exit loop
	}
}

close(INPUT)	    	|| die "can't close $configfile: $!";

# Only for debugging
# print "db_type=   $db_type\n";
# print "db_user=   $db_user\n";
# print "db_passwd= $db_passwd\n";
# print "db_server= $db_server\n";
# print "db_name=   $db_name\n";
