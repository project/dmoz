#!/usr/bin/perl
#
# Jose A. Reyero, 2005, http://www.reyero.net
#
#

use strict;


if ($#ARGV != 0){
    print "Usage: unzip.pl gzfile\n";
    print "(Unzips gzfile.)\n";
    exit;
}
my $gzfile = $ARGV[0];
my @args = ("gunzip", "--force", $gzfile);

system(@args) == 0
	 or die "system @args failed: $?";

# Everything ok
print "OK";

exit(0);