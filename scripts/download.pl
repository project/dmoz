#!/usr/bin/perl
#
# Jose A. Reyero, 2005, http://www.reyero.net
#
#

use strict;


if ($#ARGV != 1){
    print "Usage: download.pl URL folder \n";
    print "(Downloads file to folder.)\n";
    exit;
}
my $url = $ARGV[0];
my $folder = $ARGV[1];
my @args = ("wget", "--quiet", "--directory-prefix=$folder", $url );

print "Starting download of $url to $folder\n";

system(@args) == 0
	 or die "system @args failed: $?";

# Everything ok
print "OK";

exit(0);